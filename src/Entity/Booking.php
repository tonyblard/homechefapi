<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BookingRepository")
 */
class Booking
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;



    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Menu", inversedBy="bookings")
     */
    private $menu;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Period")
     */
    private $period;

    /**
     * @ORM\Column(type="integer")
     */
    private $guest;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="booking")
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $date;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Chef", inversedBy="bookings")
     * @ORM\JoinColumn(nullable=false)
     */
    private $chef;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $allergy;

    /**
     * @ORM\Column(type="float")
     */
    private $priceMenu;

    public function getId(): ?int
    {
        return $this->id;
    }





    public function getMenu(): ?Menu
    {
        return $this->menu;
    }

    public function setMenu(?Menu $menu): self
    {
        $this->menu = $menu;

        return $this;
    }

    public function getPeriod(): ?Period
    {
        return $this->period;
    }

    public function setPeriod(?Period $period): self
    {
        $this->period = $period;

        return $this;
    }

    public function getGuest(): ?int
    {
        return $this->guest;
    }

    public function setGuest(int $guest): self
    {
        $this->guest = $guest;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getDate(): ?string
    {
        return $this->date;
    }

    public function setDate(string $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getChef(): ?Chef
    {
        return $this->chef;
    }

    public function setChef(?Chef $chef): self
    {
        $this->chef = $chef;

        return $this;
    }

    public function getAllergy(): ?string
    {
        return $this->allergy;
    }

    public function setAllergy(?string $allergy): self
    {
        $this->allergy = $allergy;

        return $this;
    }

    public function getPriceMenu(): ?float
    {
        return $this->priceMenu;
    }

    public function setPriceMenu(float $priceMenu): self
    {
        $this->priceMenu = $priceMenu;

        return $this;
    }
}
