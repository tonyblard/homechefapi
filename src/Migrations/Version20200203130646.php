<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200203130646 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE localisation (id INT AUTO_INCREMENT NOT NULL, city_id INT DEFAULT NULL, range_number_id INT DEFAULT NULL, INDEX IDX_BFD3CE8F8BAC62AF (city_id), INDEX IDX_BFD3CE8F7F3078ED (range_number_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE localisation ADD CONSTRAINT FK_BFD3CE8F8BAC62AF FOREIGN KEY (city_id) REFERENCES city (id)');
        $this->addSql('ALTER TABLE localisation ADD CONSTRAINT FK_BFD3CE8F7F3078ED FOREIGN KEY (range_number_id) REFERENCES `range` (id)');
        $this->addSql('DROP TABLE chef_city');
        $this->addSql('ALTER TABLE chef DROP FOREIGN KEY FK_F24846E67F3078ED');
        $this->addSql('DROP INDEX IDX_F24846E67F3078ED ON chef');
        $this->addSql('ALTER TABLE chef DROP range_number_id');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE chef_city (chef_id INT NOT NULL, city_id INT NOT NULL, INDEX IDX_319618F68BAC62AF (city_id), INDEX IDX_319618F6150A48F1 (chef_id), PRIMARY KEY(chef_id, city_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE chef_city ADD CONSTRAINT FK_319618F6150A48F1 FOREIGN KEY (chef_id) REFERENCES chef (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE chef_city ADD CONSTRAINT FK_319618F68BAC62AF FOREIGN KEY (city_id) REFERENCES city (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE localisation');
        $this->addSql('ALTER TABLE chef ADD range_number_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE chef ADD CONSTRAINT FK_F24846E67F3078ED FOREIGN KEY (range_number_id) REFERENCES `range` (id)');
        $this->addSql('CREATE INDEX IDX_F24846E67F3078ED ON chef (range_number_id)');
    }
}
