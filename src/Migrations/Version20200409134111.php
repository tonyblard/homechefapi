<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200409134111 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE chef_localisation');
        $this->addSql('ALTER TABLE localisation ADD chef_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE localisation ADD CONSTRAINT FK_BFD3CE8F150A48F1 FOREIGN KEY (chef_id) REFERENCES chef (id)');
        $this->addSql('CREATE INDEX IDX_BFD3CE8F150A48F1 ON localisation (chef_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE chef_localisation (chef_id INT NOT NULL, localisation_id INT NOT NULL, INDEX IDX_4C983AACC68BE09C (localisation_id), INDEX IDX_4C983AAC150A48F1 (chef_id), PRIMARY KEY(chef_id, localisation_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE chef_localisation ADD CONSTRAINT FK_4C983AAC150A48F1 FOREIGN KEY (chef_id) REFERENCES chef (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE chef_localisation ADD CONSTRAINT FK_4C983AACC68BE09C FOREIGN KEY (localisation_id) REFERENCES localisation (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE localisation DROP FOREIGN KEY FK_BFD3CE8F150A48F1');
        $this->addSql('DROP INDEX IDX_BFD3CE8F150A48F1 ON localisation');
        $this->addSql('ALTER TABLE localisation DROP chef_id');
    }
}
