<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200131081719 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE chef DROP FOREIGN KEY FK_F24846E6A76ED395');
        $this->addSql('DROP INDEX UNIQ_F24846E6A76ED395 ON chef');
        $this->addSql('ALTER TABLE chef DROP user_id');
        $this->addSql('ALTER TABLE user ADD chef_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D649150A48F1 FOREIGN KEY (chef_id) REFERENCES chef (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649150A48F1 ON user (chef_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE chef ADD user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE chef ADD CONSTRAINT FK_F24846E6A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_F24846E6A76ED395 ON chef (user_id)');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D649150A48F1');
        $this->addSql('DROP INDEX UNIQ_8D93D649150A48F1 ON user');
        $this->addSql('ALTER TABLE user DROP chef_id');
    }
}
