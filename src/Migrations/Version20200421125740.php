<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200421125740 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE localisation DROP FOREIGN KEY FK_BFD3CE8F150A48F1');
        $this->addSql('DROP INDEX IDX_BFD3CE8F150A48F1 ON localisation');
        $this->addSql('ALTER TABLE localisation DROP chef_id');
        $this->addSql('ALTER TABLE chef ADD localisation_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE chef ADD CONSTRAINT FK_F24846E6C68BE09C FOREIGN KEY (localisation_id) REFERENCES localisation (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_F24846E6C68BE09C ON chef (localisation_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE chef DROP FOREIGN KEY FK_F24846E6C68BE09C');
        $this->addSql('DROP INDEX UNIQ_F24846E6C68BE09C ON chef');
        $this->addSql('ALTER TABLE chef DROP localisation_id');
        $this->addSql('ALTER TABLE localisation ADD chef_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE localisation ADD CONSTRAINT FK_BFD3CE8F150A48F1 FOREIGN KEY (chef_id) REFERENCES chef (id)');
        $this->addSql('CREATE INDEX IDX_BFD3CE8F150A48F1 ON localisation (chef_id)');
    }
}
