<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200203124247 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE `range` (id INT AUTO_INCREMENT NOT NULL, number INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE chef ADD range_number_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE chef ADD CONSTRAINT FK_F24846E67F3078ED FOREIGN KEY (range_number_id) REFERENCES `range` (id)');
        $this->addSql('CREATE INDEX IDX_F24846E67F3078ED ON chef (range_number_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE chef DROP FOREIGN KEY FK_F24846E67F3078ED');
        $this->addSql('DROP TABLE `range`');
        $this->addSql('DROP INDEX IDX_F24846E67F3078ED ON chef');
        $this->addSql('ALTER TABLE chef DROP range_number_id');
    }
}
