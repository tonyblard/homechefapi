<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200203134653 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE localisation DROP FOREIGN KEY FK_BFD3CE8F7F3078ED');
        $this->addSql('CREATE TABLE rayon (id INT AUTO_INCREMENT NOT NULL, kilometers INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('DROP TABLE `range`');
        $this->addSql('DROP INDEX IDX_BFD3CE8F7F3078ED ON localisation');
        $this->addSql('ALTER TABLE localisation DROP range_number_id');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE `range` (id INT AUTO_INCREMENT NOT NULL, kilometer INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('DROP TABLE rayon');
        $this->addSql('ALTER TABLE localisation ADD range_number_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE localisation ADD CONSTRAINT FK_BFD3CE8F7F3078ED FOREIGN KEY (range_number_id) REFERENCES `range` (id)');
        $this->addSql('CREATE INDEX IDX_BFD3CE8F7F3078ED ON localisation (range_number_id)');
    }
}
