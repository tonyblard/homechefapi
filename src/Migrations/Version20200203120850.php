<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200203120850 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE chef_city (chef_id INT NOT NULL, city_id INT NOT NULL, INDEX IDX_319618F6150A48F1 (chef_id), INDEX IDX_319618F68BAC62AF (city_id), PRIMARY KEY(chef_id, city_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE chef_city ADD CONSTRAINT FK_319618F6150A48F1 FOREIGN KEY (chef_id) REFERENCES chef (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE chef_city ADD CONSTRAINT FK_319618F68BAC62AF FOREIGN KEY (city_id) REFERENCES city (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE city CHANGE name name VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE zip CHANGE code code INT NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE chef_city');
        $this->addSql('ALTER TABLE city CHANGE name name VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE zip CHANGE code code INT DEFAULT NULL');
    }
}
