<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200131133301 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE menu_dessert');
        $this->addSql('DROP TABLE menu_dish');
        $this->addSql('DROP TABLE menu_starter');
        $this->addSql('ALTER TABLE dish ADD menu_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE dish ADD CONSTRAINT FK_957D8CB8CCD7E912 FOREIGN KEY (menu_id) REFERENCES menu (id)');
        $this->addSql('CREATE INDEX IDX_957D8CB8CCD7E912 ON dish (menu_id)');
        $this->addSql('ALTER TABLE starter ADD menu_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE starter ADD CONSTRAINT FK_4042238BCCD7E912 FOREIGN KEY (menu_id) REFERENCES menu (id)');
        $this->addSql('CREATE INDEX IDX_4042238BCCD7E912 ON starter (menu_id)');
        $this->addSql('ALTER TABLE dessert ADD menu_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE dessert ADD CONSTRAINT FK_79291B96CCD7E912 FOREIGN KEY (menu_id) REFERENCES menu (id)');
        $this->addSql('CREATE INDEX IDX_79291B96CCD7E912 ON dessert (menu_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE menu_dessert (menu_id INT NOT NULL, dessert_id INT NOT NULL, INDEX IDX_F1F20628745B52FD (dessert_id), INDEX IDX_F1F20628CCD7E912 (menu_id), PRIMARY KEY(menu_id, dessert_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE menu_dish (menu_id INT NOT NULL, dish_id INT NOT NULL, INDEX IDX_5D327CF6148EB0CB (dish_id), INDEX IDX_5D327CF6CCD7E912 (menu_id), PRIMARY KEY(menu_id, dish_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE menu_starter (menu_id INT NOT NULL, starter_id INT NOT NULL, INDEX IDX_C8993E35AD5A66CC (starter_id), INDEX IDX_C8993E35CCD7E912 (menu_id), PRIMARY KEY(menu_id, starter_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE menu_dessert ADD CONSTRAINT FK_F1F20628745B52FD FOREIGN KEY (dessert_id) REFERENCES dessert (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE menu_dessert ADD CONSTRAINT FK_F1F20628CCD7E912 FOREIGN KEY (menu_id) REFERENCES menu (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE menu_dish ADD CONSTRAINT FK_5D327CF6148EB0CB FOREIGN KEY (dish_id) REFERENCES dish (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE menu_dish ADD CONSTRAINT FK_5D327CF6CCD7E912 FOREIGN KEY (menu_id) REFERENCES menu (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE menu_starter ADD CONSTRAINT FK_C8993E35AD5A66CC FOREIGN KEY (starter_id) REFERENCES starter (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE menu_starter ADD CONSTRAINT FK_C8993E35CCD7E912 FOREIGN KEY (menu_id) REFERENCES menu (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE dessert DROP FOREIGN KEY FK_79291B96CCD7E912');
        $this->addSql('DROP INDEX IDX_79291B96CCD7E912 ON dessert');
        $this->addSql('ALTER TABLE dessert DROP menu_id');
        $this->addSql('ALTER TABLE dish DROP FOREIGN KEY FK_957D8CB8CCD7E912');
        $this->addSql('DROP INDEX IDX_957D8CB8CCD7E912 ON dish');
        $this->addSql('ALTER TABLE dish DROP menu_id');
        $this->addSql('ALTER TABLE starter DROP FOREIGN KEY FK_4042238BCCD7E912');
        $this->addSql('DROP INDEX IDX_4042238BCCD7E912 ON starter');
        $this->addSql('ALTER TABLE starter DROP menu_id');
    }
}
