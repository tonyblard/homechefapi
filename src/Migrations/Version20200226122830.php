<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200226122830 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE booking ADD period_id INT DEFAULT NULL, DROP time_start, DROP time_end');
        $this->addSql('ALTER TABLE booking ADD CONSTRAINT FK_E00CEDDEEC8B7ADE FOREIGN KEY (period_id) REFERENCES period (id)');
        $this->addSql('CREATE INDEX IDX_E00CEDDEEC8B7ADE ON booking (period_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE booking DROP FOREIGN KEY FK_E00CEDDEEC8B7ADE');
        $this->addSql('DROP INDEX IDX_E00CEDDEEC8B7ADE ON booking');
        $this->addSql('ALTER TABLE booking ADD time_start TIME NOT NULL, ADD time_end TIME NOT NULL, DROP period_id');
    }
}
