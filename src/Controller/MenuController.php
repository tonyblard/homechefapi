<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpKernel\Exception\NotAcceptableHttpException;

use App\Entity\Menu;
use App\Entity\City;
use App\Entity\Period;
use App\Entity\Disponibility;
use App\Entity\Chef;

/**
 * @Route("/menu", name="menu_")
 */

class MenuController extends AbstractController
{
  /**
   * @Route("/count", name="count", methods={"GET", "OPTIONS"})
   */
  public function countMenus()
  {
    $menus = $this->getDoctrine()->getRepository(Menu::class);
    $totalMenus = $menus->createQueryBuilder('m')
    ->select('count(m.id)')
    ->getQuery()
    ->getSingleScalarResult();


    $response = [
      'totalMenu' => $totalMenus
    ];

    return new JsonResponse($response);

  }
  private function formatMenu(Menu $menu)
  {
    return [
      'id' => $menu->getId(),
      'chef_id' => $menu->getChef()->getId(),
      'title' => $menu->getTitle(),
      'description' =>$menu->getDescription(),
      'price' => $menu->getPrice(),
    ];
  }

  /**
   * @Route("", name="list", methods={"GET", "OPTIONS"})
   */
  public function menus()
  {
    $menus = $this->getDoctrine()->getRepository(Menu::class)->findAll();
    if (!$menus) {
      throw new \Exception("Il n'y a pas de menus.");
    }

    $response = [];

    foreach ($menus as $menu) {
      $response[] = $this->formatMenu($menu);

    }
    return new JsonResponse($response);

  }

  private function toRadians($angle)
  {
    return $radian = $angle * (pi() / 180);
  }

  private function distance($latitudeSearch, $longitudeSearch, $latitudeChef, $longitudeChef )
  {
    return $distance = 6371 * acos(sin($this->toRadians($latitudeSearch)) * sin($this->toRadians($latitudeChef)) + cos($this->toRadians($latitudeSearch)) * cos($this->toRadians($latitudeChef)) * cos($this->toRadians($longitudeChef - $longitudeSearch))  );

  }

  /**
   * @Route("/search", name="search", methods={"POST", "OPTIONS"})
   */
  public function menusSearch(Request $request)
  {

    $citySearch = (string) $request->request->get('citySearch');
    $dateSearch = $request->request->get('dateSearch');
    $timeSearch = $request->request->get('timeSearch');

    $cityRequest = $this->getDoctrine()->getRepository(City::class)->findOneBy(['name' => $citySearch]);
    $latCityRequest = $cityRequest->getLatitude();
    $longCityRequest = $cityRequest->getLongitude();
    $menus = [];
    $response = [];
    if ($citySearch) {
      $chefs = $this->getDoctrine()->getRepository(Chef::class)->findAll();
      foreach ($chefs as $chef) {
        $menuChef = $chef->getMenus();
        $chefDisponibilities = $chef->getDisponibilities();
        $localisation = $chef->getLocalisation();
        $cityChef = $localisation->getCity()->getName();
        $latCityChef = $localisation->getCity()->getLatitude();
        $longCityChef = $localisation->getCity()->getLongitude();

        $distanceLong = $this->distance($latCityRequest, $longCityRequest, $latCityChef, $longCityChef);

        $rayon = $localisation->getRayon()->getKilometers();

        if ($distanceLong <= $rayon) {
          if ($dateSearch && $timeSearch === null) {

            foreach ($chefDisponibilities as $disponibility) {
              $dateDispo = $disponibility->getDate();
              $selected = $disponibility->getSelected();

              if ($dateDispo === $dateSearch && $selected === false) {

                foreach ($menuChef as $menu) {
                  $menuFormat = $this->formatMenu($menu);
                  if ( array_search($menuFormat, $response) === false) {
                    $response[] = $this->formatMenu($menu);
                  }
                }
              }
            }
          }elseif ($timeSearch && $dateSearch === null) {

            $chefDisponibilities = $chef->getDisponibilities();
            foreach ($chefDisponibilities as $disponibility) {
              $periodDispo = $disponibility->getPeriod()->getId();
              $selected = $disponibility->getSelected();
              $timeSubstr = substr($timeSearch, 0, 2);
              if ($timeSubstr === '11') {
                $idTime = 1;
              }else {
                $idTime = 2;
              }

              if ($periodDispo === $idTime && $selected === false) {
                foreach ($menuChef as $menu) {
                  $menuFormat = $this->formatMenu($menu);
                  if ( array_search($menuFormat, $response) === false) {
                    $response[] = $this->formatMenu($menu);
                  }
                }

              }
            }
          }elseif ($timeSearch && $dateSearch) {

            foreach ($chefDisponibilities as $disponibility) {
              $dateDispo = $disponibility->getDate();

              $periodDispo = $disponibility->getPeriod()->getId();
              $selected = $disponibility->getSelected();
              $timeSubstr = substr($timeSearch, 0, 2);
              if ($timeSubstr === '11') {
                $idTime = 1;
              }else {
                $idTime = 2;
              }
              if (($dateDispo === $dateSearch && $periodDispo === $idTime) && $selected === false) {

                foreach ($menuChef as $menu) {
                  $response[] = $this->formatMenu($menu);
                }
              }
            }
          }else {
            foreach ($menuChef as $menu) {
              $response[] = $this->formatMenu($menu);
            }
          }
        }
      }
    }
    return new JsonResponse($response);
  }

  /**
   * @Route("/detail/{id}", name="detail", methods={"GET", "OPTIONS"})
   */
  public function detailMenu(int $id)
  {

    $menu = $this->getDoctrine()->getRepository(Menu::class)->find($id);
    if (!$menu) {
      throw new \Exception("Le menu n'existe pas.");
    }

    $response = $this->formatMenu($menu);

   return new JsonResponse($response);
  }

  /**
   * @Route("/detail/{id}/starters", name="starters", methods={"GET", "OPTIONS"})
   */
  public function starters(int $id)
  {
    $menu = $this->getDoctrine()->getRepository(Menu::class)->find($id);
    if (!$menu) {
      throw new \Exception("Le menu n'existe pas.");
    }

    $starters = $menu->getStarters();
    if (!$starters) {
      throw new \Exception("Il n'y a pas d'entrées.");
    }

    $response = [];

    foreach ($starters as $starter) {
      $response[] = [
        'name' => $starter->getName(),
        'description' => $starter->getDescription()
      ];
    }

   return new JsonResponse($response);
  }

  /**
   * @Route("/detail/{id}/dishes", name="dishes", methods={"GET", "OPTIONS"})
   */
  public function dishes(int $id)
  {
    $menu = $this->getDoctrine()->getRepository(Menu::class)->find($id);
    if (!$menu) {
      throw new \Exception("Le menu n'existe pas.");
    }

    $dishes = $menu->getDishes();
    if (!$dishes) {
      throw new \Exception("Il n'y a pas de plats.");
    }


    $response = [];
    foreach ($dishes as $dish) {
      $response[] = [
        'name' => $dish->getName(),
        'description' => $dish->getDescription()
      ];
    }

   return new JsonResponse($response);
  }

  /**
   * @Route("/detail/{id}/desserts", name="desserts", methods={"GET", "OPTIONS"})
   */
  public function desserts(int $id)
  {
    $menu = $this->getDoctrine()->getRepository(Menu::class)->find($id);
    if (!$menu) {
      throw new \Exception("Le menu n'existe pas.");
    }

    $desserts = $menu->getDesserts();
    if (!$desserts) {
      throw new \Exception("Il n'y a pas de desserts.");
    }

    $response = [];

    foreach ($desserts as $dessert) {
      $response[] = [
        'name' => $dessert->getName(),
        'description' => $dessert->getDescription()
      ];
    }
   return new JsonResponse($response);
  }


  /**
   * @Route("/detail/{id}/categories", name="categories", methods={"GET", "OPTIONS"})
   */
  public function categories(int $id)
  {
    $menu = $this->getDoctrine()->getRepository(Menu::class)->find($id);
    if (!$menu) {
      throw new \Exception("Le menu n'existe pas.");
    }

    $categories = $menu->getCategories();
    if (!$categories) {
      throw new \Exception("Il n'y a pas de catégories.");
    }

    $response = [];
    foreach ($categories as $category) {
      $response[] = [
        'name' => $category->getName()
      ];
    }
   return new JsonResponse($response);
  }


  /**
   * @Route("/detail/{id}/diets", name="diets", methods={"GET", "OPTIONS"})
   */
  public function diets(int $id)
  {
    $menu = $this->getDoctrine()->getRepository(Menu::class)->find($id);
    if (!$menu) {
      throw new \Exception("Le menu n'existe pas.");
    }

    $diets = $menu->getDiets();
    if (!$diets) {
      throw new \Exception("Il n'y a pas de regime alimentaire.");
    }

    $response = [];

    foreach ($diets as $diet) {
      $response[] = [
        'name' => $diet->getName()
      ];
    }
   return new JsonResponse($response);
  }


   /**
    * @Route("/detail/{id}/bookings", name="booking", methods={"GET", "OPTIONS"})
    */
   public function allDisponibilities(int $id)
   {
       $menu = $this->getDoctrine()->getRepository(Menu::class)->findOneby(['id' => $id]);
       $chef = $menu->getChef();

       $disponibilities = $this->getDoctrine()->getRepository(Disponibility::class)->findBy(['chef' => $chef]);

       $response = [];
       foreach ($disponibilities as $disponibility) {
         $selected = $disponibility->getSelected();
         if ($selected === false) {
           $response[] = [
             'id' => $disponibility->getId(),
             'date' => $disponibility->getDate(),
             'period' => $disponibility->getPeriod()->getId(),
             'selected' => $disponibility->getSelected(),
           ];
         }

       }
       return new JsonResponse($response);
   }


  /**
   * @Route("/detail/{id}/notes", name="note", methods={"GET", "OPTIONS"})
   */
  public function notes(int $id)
  {
    $menu = $this->getDoctrine()->getRepository(Menu::class)->find($id);
    if (!$menu) {
        throw new \Exception("Le menu n'existe pas.");
    }

    $notes = $menu->getNotes();
    if (!$notes) {
        throw new \Exception("Il n'y a pas de notes.");
    }

    $response = [];

    foreach ($notes as $note) {
        $response[] = [
            'review' => $note->getReview(),
            'date' => $note->getDate()->format('d/m/Y H:i'),
            'comment' => $note->getComment(),
            'id' => $note->getId()
        ];
    }
   return new JsonResponse($response);
  }

}
