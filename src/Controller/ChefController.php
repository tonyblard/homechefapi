<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpKernel\Exception\NotAcceptableHttpException;

use App\Entity\Chef;
use App\Entity\User;

/**
 * @Route("/chef", name="chef_")
 */

class ChefController extends AbstractController
{

  private function formatChef(Chef $chef)
  {
    return [
        'id' => $chef->getId(),
        'userId' => $chef->getUser()->getId(),
        'photo' => $chef->getPhoto(),
        'bio' => $chef->getBio()
    ];
  }

  /**
   * @Route("/count", name="count", methods={"GET", "OPTIONS"})
   */
  public function countChef()
  {
    $chef = $this->getDoctrine()->getRepository(Chef::class);
    $totalChef = $chef->createQueryBuilder('c')
    ->select('count(c.id)')
    ->getQuery()
    ->getSingleScalarResult();


    $response = [
      'totalChef' => $totalChef
    ];

    return new JsonResponse($response);

  }

  /**
   * @Route("/list", name="list", methods={"GET", "OPTIONS"})
   */
  public function chefs()
  {
    $chefs = $this->getDoctrine()->getRepository(Chef::class)->findAll();
    if (!$chefs) {
      throw new \Exception("Il n'y a pas de chefs.");
    }
    $response = [];

    foreach ($chefs as $chef) {
      $response[] = $this->formatChef($chef);
    }
    return new JsonResponse($response);

  }

  /**
   * @Route("/{id}", name="detail", methods={"GET", "OPTIONS"})
   */
  public function detailInfoChef(int $id)
  {
    $chef = $this->getDoctrine()->getRepository(Chef::class)->find($id);
    if (!$chef) {
      throw new \Exception("Le chef n'existe pas.");
    }

    $response = $this->formatChef($chef);

   return new JsonResponse($response);
  }

  /**
   * @Route("/user/{id}", name="detail_user", methods={"GET", "OPTIONS"})
   */
  public function detailUserChef(int $id)
  {
    $chef = $this->getDoctrine()->getRepository(Chef::class)->find($id);
    if (!$chef) {
      throw new \Exception("Le chef n'existe pas.");
    }
    $user = $chef->getUser();
    $response = [
      'id' => $user->getId(),
      'name' => $user->getName(),
      'phone' => $user->getPhone(),
      'email' => $user->getEmail()
    ];

   return new JsonResponse($response);
  }


  /**
   * @Route("/{id}/localisation", name="localisation", methods={"GET", "OPTIONS"})
   */
  public function localisation(int $id)
  {
    $chef = $this->getDoctrine()->getRepository(Chef::class)->find($id);
    if (!$chef) {
        throw new \Exception("Le chef n'existe pas.");
    }

    $localisation = $chef->getLocalisation();
    if (!$localisation) {
        throw new \Exception("Il n'y a pas de localisations.");
    }


    $response = [
        'city' => $localisation->getCity()->getName(),
        'rangeNumber' => $localisation->getRayon()->getKilometers(),
        'id' => $localisation->getId()
    ];


    return new JsonResponse($response);
  }

  /**
   * @Route("/{id}/menus", name="menus", methods={"GET", "OPTIONS"})
   */
  public function menus(int $id)
  {
    $chef = $this->getDoctrine()->getRepository(Chef::class)->find($id);
    if (!$chef) {
        throw new \Exception("Le chef n'existe pas.");
    }

    $menus = $chef->getMenus();
    if (!$menus) {
        throw new \Exception("Il n'y a pas de menus.");
    }

    $response = [];

    foreach ($menus as $menu) {
        $response[] = [
            'title' => $menu->getTitle(),
            'description' => $menu->getDescription(),
            'id' => $menu->getId()
        ];
    }

    return new JsonResponse($response);
  }

}
