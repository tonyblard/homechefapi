<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpKernel\Exception\NotAcceptableHttpException;

use App\Entity\Booking;

/**
 * @Route("/booking", name="booking_")
 */

class BookingController extends AbstractController
{

  private function formatDateEnglish(String $date)
  {
    $year = substr($date, 6, 4);
    $month = substr($date, 3, 2);
    $day = substr($date, 0, 2);
    return "$month/$day/$year";
  }

  /**
   * @Route("/count", name="count", methods={"GET", "OPTIONS"})
   */
  public function countBooking()
  {
    $bookings  = $this->getDoctrine()->getRepository(Booking::class)->findAll();
    $count = 0;
    foreach ($bookings as $booking) {
      $idPeriod = $booking->getPeriod()->getId();

      if ($idPeriod === 1) {
        $period = "11:00:00";
      }else {
        $period = "18:00:00";
      }

      $today = date("d/m/Y h:i:sa");
      $date = $booking->getDate();

      $dateEnglish = $this->formatDateEnglish($date);

      $strtotime = strtotime($dateEnglish ." " . $period);

      $dateFormat = date("d/m/Y h:i:sa", $strtotime);
      if ($dateFormat <= $today) {
        $count = $count +1;
      }

    }
    $response = [
      'totalBooking' => $count
    ];
    return new JsonResponse($response);

  }

}
