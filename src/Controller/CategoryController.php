<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpKernel\Exception\NotAcceptableHttpException;

use App\Entity\Category;

/**
 * @Route("/categories", name="category_")
 */

class CategoryController extends AbstractController
{
  /**
   * @Route("/list", name="list", methods={"GET", "OPTIONS"})
   */
  public function allCategories()
  {
      $categories = $this->getDoctrine()->getRepository(Category::class)->findAll();

      $response = [];
      foreach ($categories as $category) {
        $response[] = [
          'id' => $category->getId(),
          'name' => $category->getName(),
        ];
      }
      return new JsonResponse($response);
  }

}
