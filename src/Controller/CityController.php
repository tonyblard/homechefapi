<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpKernel\Exception\NotAcceptableHttpException;

use App\Entity\City;
use App\Entity\Menu;
/**
 * @Route("/city", name="city_")
 */

class CityController extends AbstractController
{

  /**
   * @Route("/list", name="list", methods={"GET", "OPTIONS"})
   */
  public function cities()
  {
    $cities = $this->getDoctrine()->getRepository(City::class)->findAll();

    $response = [];

    foreach ($cities as $city) {
      $response[] = [
        'name' => $city->getName(),
        'id' => $city->getId()
      ];
    }
    return new JsonResponse($response);

  }

  private function toRadians($angle)
  {
    return $radian = $angle * (pi() / 180);
  }

  private function distance($latitudeSearch, $longitudeSearch, $latitudeChef, $longitudeChef )
  {
    return $distance = 6371 * acos(sin($this->toRadians($latitudeSearch)) * sin($this->toRadians($latitudeChef)) + cos($this->toRadians($latitudeSearch)) * cos($this->toRadians($latitudeChef)) * cos($this->toRadians($longitudeChef - $longitudeSearch))  );

  }

  /**
   * @Route("/booking/{id}", name="booking", methods={"GET", "OPTIONS"})
   */
  public function citiesBooking(int $id)
  {
    $menu = $this->getDoctrine()->getRepository(Menu::class)->findOneby(['id' => $id]);
    $chef = $menu->getChef();
    $localisation = $chef->getLocalisation();
    $cityChef = $localisation->getCity();
    $rayon = $localisation->getRayon()->getKilometers();

    $latChef = $cityChef->getLatitude();
    $longChef = $cityChef->getLongitude();


    $cities = $this->getDoctrine()->getRepository(City::class)->findAll();

    $response = [];

    foreach ($cities as $city) {
      $latCity = $city->getLatitude();
      $longCity = $city->getLongitude();
      $distanceCities = $this->distance($latChef, $longChef, $latCity, $longCity);

      if ($distanceCities <= $rayon) {
        $response[] = [
          'name' => $city->getName(),
          'id' => $city->getId()
        ];
      }

    }
    return new JsonResponse($response);

  }


}
