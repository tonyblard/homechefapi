<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Firebase\JWT\JWT;

class SecurityController extends AbstractController
{
  /**
   * @Route("/google", name="google_login", methods={"POST", "OPTIONS"} )
   */
  public function loginGoogle(): JsonResponse
  {
      $user = $this->getUser();
      if ($user->getEmail() === "login.google@test.com") {
        $jwt = 10;
      }else {
        $jwt = JWT::encode([
          'sub' => $user->getId(),
        ], 'dev');
      }



    return $this->json($jwt);
  }
}
