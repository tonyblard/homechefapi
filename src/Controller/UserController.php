<?php

namespace App\Controller;

use App\Entity\Adress;
use App\Entity\Booking;
use App\Entity\Category;
use App\Entity\Chef;
use App\Entity\City;
use App\Entity\Dessert;
use App\Entity\Diet;
use App\Entity\Dish;
use App\Entity\Disponibility;
use App\Entity\Localisation;
use App\Entity\Menu;
use App\Entity\Period;
use App\Entity\Rayon;
use App\Entity\Starter;
use App\Entity\User;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpKernel\Exception\NotAcceptableHttpException;



/**
 * @Route("/user", name="user_")
 * @isGranted("ROLE_USER")
 */
class UserController extends AbstractController
{
    /**
     * @Route("/me", name="detail", methods={"GET", "OPTIONS"})
     */
    public function detailUser()
    {
      $user = $this->getUser();
      if (!$user) {
          throw new \Exception("Vous n'êtes pas connecté");
      }

      $response = [
        'email' => $user->getEmail(),
        'name' => $user->getName(),
        'birthday' => $user->getBirthday(),
        'charte' => $user->getCharte(),
        'phone' => $user->getPhone(),
        'chef_id' => $user->getChef(),
      ];

     return new JsonResponse($response);
    }


    /**
     * @Route("/me/address", name="detail_adress", methods={"GET", "OPTIONS"})
     */
    public function adressUser()
    {
        $user = $this->getUser();
        if (!$user) {
            throw new \Exception("Vous n'êtes pas connecté");
        }
        $adress = $user->getAdress();
        if (!$adress) {
          $response = [
              'street' => null,
              'city' => null,
              'zip' => null,
          ];
        }else {
          $response = [
              'street' => $adress->getStreet(),
              'city' => $adress->getCity()->getName(),
              'zip' => $adress->getCity()->getZip()->getCode(),
          ];
        }


        return new JsonResponse($response);
    }

    /**
     * @Route("/me/booking", name="booking_user", methods={"GET", "OPTIONS"})
     */
    public function bookingsUser()
    {
        $user = $this->getUser();
        if (!$user) {
            throw new \Exception("Vous n'êtes pas connecté");
        }
        //$bookings = $user->getBooking();
        $bookings  = $this->getDoctrine()->getRepository(Booking::class)->findBy(['user'=> $user], ['date' => 'DESC', 'period' => 'DESC']);
        $response = [];
        foreach ($bookings as $booking) {
          $idPeriod = $booking->getPeriod()->getId();

          if ($idPeriod === 1) {
            $period = "11:00:00";
          }else {
            $period = "18:00:00";
          }

          $today = date("d/m/Y h:i:sa");
          $strtotimeToday = strtotime($today);
          $date = $booking->getDate();

          $dateEnglish = $this->formatDateEnglish($date);

          $strtotime = strtotime($dateEnglish ." " . $period);

          $dateFormat = date("d/m/Y h:i:sa", $strtotime);

          if ($strtotime > $strtotimeToday) {
            $response[] = [
              'id' => $booking->getId(),
              'idMenu' => $booking->getMenu()->getId(),
              'titleMenu' => $booking->getMenu()->getTitle(),
              'descriptionMenu' => $booking->getMenu()->getDescription(),
              'idPeriod' => $booking->getPeriod()->getId(),
              'guest' => $booking->getGuest(),
              'date' => $booking->getDate(),
              'emailChef' => $booking->getChef()->getUser()->getEmail(),
              'phoneChef' => $booking->getChef()->getUser()->getPhone(),
              'priceMenu' => $booking->getPriceMenu(),
              'allergy' => $booking->getAllergy(),
            ];
          }

        }
        return new JsonResponse($response);
    }

    /**
     * @Route("/me/booking/past", name="bookings_past_user", methods={"GET", "OPTIONS"})
     */
    public function bookingsPastUser()
    {
        $user = $this->getUser();
        if (!$user) {
            throw new \Exception("Vous n'êtes pas connecté");
        }
        //$bookings = $user->getBooking();
        $bookings  = $this->getDoctrine()->getRepository(Booking::class)->findBy(['user'=> $user], ['date' => 'DESC', 'period' => 'DESC']);
        $response = [];
        foreach ($bookings as $booking) {
          $idPeriod = $booking->getPeriod()->getId();

          if ($idPeriod === 1) {
            $period = "11:00:00";
          }else {
            $period = "18:00:00";
          }

          $today = date("d/m/Y h:i:sa");
          $strtotimeToday = strtotime($today);
          $date = $booking->getDate();

          $dateEnglish = $this->formatDateEnglish($date);

          $strtotime = strtotime($dateEnglish ." " . $period);

          $dateFormat = date("d/m/Y h:i:sa", $strtotime);
          if ($strtotime <= $strtotimeToday) {
            $response[] = [
              'id' => $booking->getId(),
              'idMenu' => $booking->getMenu()->getId(),
              'titleMenu' => $booking->getMenu()->getTitle(),
              'descriptionMenu' => $booking->getMenu()->getDescription(),
              'idPeriod' => $booking->getPeriod()->getId(),
              'guest' => $booking->getGuest(),
              'date' => $booking->getDate(),
              'emailChef' => $booking->getChef()->getUser()->getEmail(),
              'phoneChef' => $booking->getChef()->getUser()->getPhone(),
              'priceMenu' => $booking->getPriceMenu(),
              'allergy' => $booking->getAllergy(),
            ];
          }

        }
        return new JsonResponse($response);
    }

    /**
     * @Route("/me/chef", name="detail_chef", methods={"GET", "OPTIONS"})
     */
    public function detailChef()
    {
      $user = $this->getUser();
      if ($user->getChef() === null) {
        throw new \Exception("Vous n'êtes pas chef");
      }
      $chef = $user->getChef();

      $response = [
        'bio' => $chef->getBio(),
        'photo' => $chef->getPhoto(),
        'id_chef' => $chef->getId()
      ];

     return new JsonResponse($response);
    }

    /**
     * @Route("/me/chef/localisations", name="chef_localisations", methods={"GET", "OPTIONS"})
     */
    public function chefLocalisations()
    {
      $user = $this->getUser();
      if ($user->getChef() === null) {
        throw new \Exception("Vous n'êtes pas chef");
      }
      $chef = $user->getChef();
      $localisation = $chef->getLocalisation();

      if (!$localisation) {
        $response = [
          'id' => null,
          'cityLocalisation' => null,
          'rayon' => null
        ];
      }else {
        $response = [
          'id' => $localisation->getId(),
          'cityLocalisation' => $localisation->getCity()->getName(),
          'rayon' => $localisation->getRayon()->getKilometers()
        ];
      }

     return new JsonResponse($response);
    }


    private function formatMenu(Menu $menu)
    {
      return [
        'id' => $menu->getId(),
        'title' => $menu->getTitle(),
        'description' =>$menu->getDescription(),
      ];
    }


    /**
     * @Route("/me/chef/menu", name="detail_chef_menu", methods={"GET", "OPTIONS"})
     */
    public function detailChefMenu()
    {
        $user = $this->getUser();

        if ($user->getChef() === null) {
          throw new \Exception("Vous n'êtes pas chef");
        }
        $chef = $user->getChef();

        $menus = $chef->getMenus();

        $response = [];
        foreach ($menus as $menu) {
            $response[] = $this->formatMenu($menu);
        }
        return new JsonResponse($response);
    }

    /**
     * @Route("/me/chef/detail/menu/{idMenu}", name="detail_chef_menu_one", methods={"GET", "OPTIONS"})
     */
    public function detailChefMenuOne(int $idMenu)
    {
        $user = $this->getUser();

        if ($user->getChef() === null) {
          throw new \Exception("Vous n'êtes pas chef");
        }
        $chef = $user->getChef();

        $menu =  $this->getDoctrine()->getRepository(Menu::class)->find($idMenu);
        $starter = $this->getDoctrine()->getRepository(Starter::class)->findOneBy(["menu" => $menu]);
        $dish = $this->getDoctrine()->getRepository(Dish::class)->findOneBy(["menu" => $menu]);
        $dessert = $this->getDoctrine()->getRepository(Dessert::class)->findOneBy(["menu" => $menu]);

        $response = [
          'id' => $menu->getId(),
          'title' => $menu->getTitle(),
          'description' => $menu->getDescription(),
          'price' => $menu->getPrice(),
          'idStarter' => $starter->getId(),
          'titleStarter' => $starter->getName(),
          'descriptionStarter' => $starter->getDescription(),
          'idDish' => $dish->getId(),
          'titleDish' => $dish->getName(),
          'descriptionDish' => $dish->getDescription(),
          'idDessert' => $dessert->getId(),
          'titleDessert' => $dessert->getName(),
          'descriptionDessert' => $dessert->getDescription(),
        ];
        return new JsonResponse($response);
    }

    /**
     * @Route("/me/chef/detail/menu/{idMenu}/diets", name="detail_menu_diets", methods={"GET", "OPTIONS"})
     */
    public function detailMenuDiets(int $idMenu)
    {
        $user = $this->getUser();

        if ($user->getChef() === null) {
          throw new \Exception("Vous n'êtes pas chef");
        }
        $chef = $user->getChef();

        $menu =  $this->getDoctrine()->getRepository(Menu::class)->find($idMenu);
        $diets = $menu->getDiets();
        $response = [];
        foreach ($diets as $diet) {
          $response[] = [
            'id'=> $diet->getId(),
            'name' =>$diet->getName(),
          ];
        }
        return new JsonResponse($response);
    }

    /**
     * @Route("/me/chef/detail/menu/{idMenu}/categories", name="detail_menu_categories", methods={"GET", "OPTIONS"})
     */
    public function detailMenuCategories(int $idMenu)
    {
        $user = $this->getUser();

        if ($user->getChef() === null) {
          throw new \Exception("Vous n'êtes pas chef");
        }
        $chef = $user->getChef();

        $menu =  $this->getDoctrine()->getRepository(Menu::class)->find($idMenu);
        $categories = $menu->getCategories();
        $response = [];
        foreach ($categories as $category) {
          $response[] = [
            'id' => $category->getId(),
            'name' => $category->getName(),
          ];
        }
        return new JsonResponse($response);
    }

    /**
     * @Route("/me/chef/menu/diets", name="menu_diets", methods={"GET", "OPTIONS"})
     */
    public function diets()
    {
        $user = $this->getUser();

        if ($user->getChef() === null) {
          throw new \Exception("Vous n'êtes pas chef");
        }
        $diets = $this->getDoctrine()->getRepository(Diet::class)->findAll();

        $response = [];
        foreach ($diets as $diet) {
          $response[] = [
            'id' => $diet->getId(),
            'name' => $diet->getName(),
          ];
        }
        return new JsonResponse($response);
    }

    /**
     * @Route("/me/chef/menu/categories", name="menu_categories", methods={"GET", "OPTIONS"})
     */
    public function categories()
    {
        $user = $this->getUser();

        if ($user->getChef() === null) {
          throw new \Exception("Vous n'êtes pas chef");
        }
        $categories = $this->getDoctrine()->getRepository(Category::class)->findAll();

        $response = [];
        foreach ($categories as $category) {
          $response[] = [
            'id' => $category->getId(),
            'name' => $category->getName(),
          ];
        }
        return new JsonResponse($response);
    }

    /**
     * @Route("/me/chef/period", name="chef_period", methods={"GET", "OPTIONS"})
     */
    public function periods()
    {
        $user = $this->getUser();

        if ($user->getChef() === null) {
          throw new \Exception("Vous n'êtes pas chef");
        }
        $periods = $this->getDoctrine()->getRepository(Period::class)->findAll();

        $response = [];
        foreach ($periods as $period) {
          $response[] = [
            'id' => $period->getId(),
            'start' => $period->getTimeStart(),
            'end' => $period->getTimeEnd(),
          ];
        }
        return new JsonResponse($response);
    }

    private function formatDateEnglish(String $date)
    {
      $year = substr($date, 6, 4);
      $month = substr($date, 3, 2);
      $day = substr($date, 0, 2);
      return "$month/$day/$year";
    }

    /**
     * @Route("/me/chef/booking", name="booking_chef", methods={"GET", "OPTIONS"})
     */
    public function bookingsChef()
    {
        $user = $this->getUser();

        if ($user->getChef() === null) {
          throw new \Exception("Vous n'êtes pas chef");
        }
        $chef = $user->getChef();

        //$bookings = $chef->getBookings();
        $bookings  = $this->getDoctrine()->getRepository(Booking::class)->findBy(['chef'=> $chef], ['date' => 'DESC', 'period' => 'DESC']);

        $response = [];
        foreach ($bookings as $booking) {
          $idPeriod = $booking->getPeriod()->getId();

          if ($idPeriod === 1) {
            $period = "11:00:00";
          }else {
            $period = "18:00:00";
          }

          $today = date("d/m/Y h:i:sa");
          $strtotimeToday = strtotime($today);
          
          $date = $booking->getDate();

          $dateEnglish = $this->formatDateEnglish($date);

          $strtotime = strtotime($dateEnglish ." " . $period);

          if ($strtotime > $strtotimeToday) {
            $response[] = [
              'id' => $booking->getId(),
              'idMenu' => $booking->getMenu()->getId(),
              'titleMenu' => $booking->getMenu()->getTitle(),
              'descriptionMenu' => $booking->getMenu()->getDescription(),
              'idPeriod' => $booking->getPeriod()->getId(),
              'guest' => $booking->getGuest(),
              'date' => $booking->getDate(),
              'emailUser' => $booking->getUser()->getEmail(),
              'phoneUser' => $booking->getUser()->getPhone(),
              'price' => $booking->getPriceMenu(),
              'allergy' => $booking->getAllergy(),
            ];
          }

        }
        return new JsonResponse($response);
    }

    /**
     * @Route("/me/chef/booking/past", name="booking_past_chef", methods={"GET", "OPTIONS"})
     */
    public function bookingsPastChef()
    {
        $user = $this->getUser();

        if ($user->getChef() === null) {
          throw new \Exception("Vous n'êtes pas chef");
        }
        $chef = $user->getChef();

        //$bookings = $chef->getBookings();
        $bookings  = $this->getDoctrine()->getRepository(Booking::class)->findBy(['chef'=> $chef], ['date' => 'DESC', 'period' => 'DESC']);

        $response = [];
        foreach ($bookings as $booking) {
          $idPeriod = $booking->getPeriod()->getId();

          if ($idPeriod === 1) {
            $period = "11:00:00";
          }else {
            $period = "18:00:00";
          }

          $today = date("d/m/Y h:i:sa");

          $strtotimeToday = strtotime($today);
          $date = $booking->getDate();

          $dateEnglish = $this->formatDateEnglish($date);

          $strtotime = strtotime($dateEnglish ." " . $period);

          if ($strtotime <= $strtotimeToday) {
            $response[] = [
              'id' => $booking->getId(),
              'idMenu' => $booking->getMenu()->getId(),
              'titleMenu' => $booking->getMenu()->getTitle(),
              'descriptionMenu' => $booking->getMenu()->getDescription(),
              'idPeriod' => $booking->getPeriod()->getId(),
              'guest' => $booking->getGuest(),
              'date' => $booking->getDate(),
              'emailUser' => $booking->getUser()->getEmail(),
              'phoneUser' => $booking->getUser()->getPhone(),
              'price' => $booking->getPriceMenu(),
              'allergy' => $booking->getAllergy(),
            ];
          }

        }
        return new JsonResponse($response);
    }

    /**
     * @Route("/me/chef/disponibilities", name="chef_disponibilities", methods={"GET", "OPTIONS"})
     */
    public function getDisponibilities()
    {
        $user = $this->getUser();

        if ($user->getChef() === null) {
          throw new \Exception("Vous n'êtes pas chef");
        }
        $chef = $user->getChef();
        $chefDisponibilities = $this->getDoctrine()->getRepository(Disponibility::class)->findBy(['chef' => $chef]);

        $response = [];
        foreach ($chefDisponibilities as $disponibility) {
          $response[] = [
            'id' => $disponibility->getId(),
            'period_id' => $disponibility->getPeriod()->getId(),
            'date' => $disponibility->getDate(),
            'selected' => $disponibility->getSelected()
          ];
        }
        return new JsonResponse($response);
    }

    /**
     * @Route("/me/add/info", name="add_info", methods={"POST", "OPTIONS"})
     */
    public function addDetailUser(Request $request)
    {
        $user = $this->getUser();
        if (!$user) {
            throw new \Exception("Vous n'êtes pas connecté");
        }

        $name = $request->request->get('name');
        $email = $request->request->get('email');
        $birthday = $request->request->get('birthday');
        $phone = $request->request->get('phone');

        $user->setName($name);
        $user->setEmail($email);
        $user->setBirthday($birthday);
        $user->setPhone($phone);

        $manager = $this->getDoctrine()->getManager();
        $manager->persist($user);
        $manager->flush();

        return $this->json([
            'success' => true,
            'idUser' => $user->getId()
        ]);
    }

    /**
     * @Route("/me/add/address", name="add_adress", methods={"POST", "OPTIONS"})
     */
    public function addAdressUser(Request $request)
    {
        $user = $this->getUser();
        if (!$user) {
            throw new \Exception("Vous n'êtes pas connecté");
        }

        $street = $request->request->get('street');
        $cityName = $request->request->get('city');
        $city = $this->getDoctrine()->getRepository(City::class)->findOneBy(['name' => $cityName]);

        $adress = $user->getAdress();
        if ($adress) {
            $adress->setStreet($street);
            $adress->setCity($city);

            $manager = $this->getDoctrine()->getManager();
            $manager->persist($adress);

            $user->setAdress($adress);

            $manager->persist($user);
            $manager->flush();
        }else {
            $adress = new Adress;
            $adress->setStreet($street);
            $adress->setCity($city);

            $manager = $this->getDoctrine()->getManager();
            $manager->persist($adress);

            $user->setAdress($adress);

            $manager->persist($user);
            $manager->flush();
        }


        return $this->json([
            'success' => true,
            'idUser' => $user->getId(),
            'idAdress' => $adress->getId()
        ]);
    }

    /**
     * @Route("/me/add/booking", name="add_booking", methods={"POST", "OPTIONS"})
     */
    public function addBooking(Request $request)
    {
        $user = $this->getUser();
        if (!$user) {
            throw new \Exception("Vous n'êtes pas connecté");
        }

        $menuId = (int) $request->request->get('menuId');
        $periodId = (int) $request->request->get('periodId');
        $guest = (int) $request->request->get('guest');
        $date = (string) $request->request->get('date');
        $allergy = (string) $request->request->get('allergy');
        $price = (float) $request->request->get('price');
        $menu = $this->getDoctrine()->getRepository(Menu::class)->findOneBy(['id' => $menuId]);
        $period = $this->getDoctrine()->getRepository(Period::class)->findOneBy(['id' => $periodId]);
        $chef = $menu->getChef();

        $disponibility = $this->getDoctrine()->getRepository(Disponibility::class)->findOneBy(['date' => $date, 'period' => $period]);
        $booking = new Booking;
        $booking->setDate($date);
        $booking->setUser($user);
        $booking->setGuest($guest);
        $booking->setPeriod($period);
        $booking->setMenu($menu);
        $booking->setChef($chef);
        $booking->setPriceMenu($price);
        if ($allergy) {
          $booking->setAllergy($allergy);
        }
        $disponibility->setSelected(true);

        $manager = $this->getDoctrine()->getManager();
        $manager->persist($booking);
        $manager->persist($disponibility);
        $manager->flush();

        return $this->json([
            'success' => true,
            'idUser' => $user->getId(),
            'idBooking' => $booking->getId()
        ]);
    }

    /**
     * @Route("/me/remove/booking", name="remove_booking", methods={"POST", "OPTIONS"})
     */
    public function removeBooking(Request $request)
    {
        $user = $this->getUser();
        if (!$user) {
            throw new \Exception("Vous n'êtes pas connecté");
        }
        $idBooking = (int) $request->request->get('idBooking');
        $booking = $this->getDoctrine()->getRepository(Booking::class)->find($idBooking);

        $date = $booking->getDate();
        $period = $booking->getPeriod();
        if (!$booking) {
            return $this->json([
                'success' => false,
                'message' => "Cette réservation n'existe pas.",
            ]);
        }

        $disponibility = $this->getDoctrine()->getRepository(Disponibility::class)->findOneBy(['date' => $date, 'period' => $period]);
        $manager = $this->getDoctrine()->getManager();
        $disponibility->setSelected(false);
        $manager->remove($booking);
        $manager->persist($disponibility);
        $manager->flush();

        return $this->json([
            'success' => true,
            'bookingId' => $idBooking,
        ]);
    }

    /**
     * @Route("/me/chef/add/info", name="chef_add_info", methods={"POST", "OPTIONS"})
     */
    public function addChefInfo(Request $request)
    {
        $user = $this->getUser();

        $bio = (string) $request->request->get('bio');

        if (!$bio) {
            throw new NotAcceptableHttpException('Invalid param bio');
        }
        $photo = (string) $request->request->get('photo');
        if (!$photo) {
            throw new NotAcceptableHttpException('Invalid param photo');
        }
        $chef = $user->getChef();
        if (!$chef) {
            $chef = new Chef;
            $chef->setBio($bio);
            $chef->setPhoto($photo);
            $manager = $this->getDoctrine()->getManager();
            $manager->persist($chef);

            $user->setChef($chef);
            $manager->persist($user);

            $manager->flush();
        }else {
            $chef->setBio($bio);
            $chef->setPhoto($photo);
            $manager = $this->getDoctrine()->getManager();
            $manager->persist($chef);

            $user->setChef($chef);

            $manager->persist($user);
            $manager->flush();

        }
        return $this->json([
            'success' => true,
            'idChef' => $chef->getId()
        ]);
    }

    /**
     * @Route("/me/chef/disponibility/add", name="add_disponibility", methods={"POST", "OPTIONS"})
     */
    public function addDisponibility(Request $request)
    {
        $user = $this->getUser();
        $chef = $user->getChef();
        if (!$chef) {
            throw new NotAcceptableHttpException("Vous n'êtes pas chef");
        }

        $date = (string) $request->request->get('date');
        $idPeriod = (int) $request->request->get('idPeriod');
        $select = (int) $request->request->get('select');

        if (!$date) {
            throw new NotAcceptableHttpException('Invalid param date');
        }
        if (!$idPeriod) {
            throw new NotAcceptableHttpException('Invalid param idPeriod');
        }

        $period = $this->getDoctrine()->getRepository(Period::class)->findOneBy(['id' => $idPeriod]);

        $disponibility = new Disponibility;
        $disponibility->setDate($date);
        $disponibility->setPeriod($period);
        $disponibility->setSelected($select);
        $disponibility->setChef($chef);
        $manager = $this->getDoctrine()->getManager();
        $manager->persist($disponibility);
        $manager->flush();

        return $this->json([
            'success' => true,
            'idDisponibility' => $disponibility->getId()
        ]);
    }

    /**
     * @Route("/me/chef/disponibility/remove", name="remove_disponibility", methods={"POST", "OPTIONS"})
     */
    public function removeDisponibility(Request $request)
    {
        $user = $this->getUser();
        $chef = $user->getChef();
        if (!$chef) {
            throw new \Exception("Vous n'êtes pas chef");
        }

        $date = (string) $request->request->get('date');
        $idPeriod = (int) $request->request->get('idPeriod');
        $period = $this->getDoctrine()->getRepository(Period::class)->findOneBy(['id' => $idPeriod]);
        $disponibility = $this->getDoctrine()->getRepository(Disponibility::class)->findOneBy(['date' => $date, 'period' => $period]);

        $disponibilityId = $disponibility->getId();
        $chefDisponibilities = $chef->getDisponibilities();
        foreach ($chefDisponibilities as $chefDisponibility) {
            if ($disponibilityId == $chefDisponibility->getId()) {
                $removeDisponibility = $chefDisponibility;
            }
        }
        if (!$removeDisponibility) {
            return $this->json([
                'success' => false,
                'message' => "Cette disponibilité n'existe pas.",
            ]);
        }

        $chef->removeDisponibility($removeDisponibility);
        $manager = $this->getDoctrine()->getManager();
        $manager->remove($removeDisponibility);
        $manager->flush();

        return $this->json([
            'success' => true,
            'disponibilityId' => $disponibilityId,
        ]);
    }

    /**
     * @Route("/me/chef/localisation/add", name="add_localisation", methods={"POST", "OPTIONS"})
     */
    public function addLocalisation(Request $request)
    {
        $user = $this->getUser();
        $chef = $user->getChef();
        if (!$chef) {
            throw new NotAcceptableHttpException("Vous n'êtes pas chef");
        }

        $cityName = (string) $request->request->get('cityName');
        $rayonNumber = (int) $request->request->get('rayon');

        if (!$cityName) {
            throw new NotAcceptableHttpException('Invalid param city');
        }
        if (is_int($rayonNumber) === false) {
            throw new NotAcceptableHttpException('Invalid param rayon');
        }

        $city = $this->getDoctrine()->getRepository(City::class)->findOneBy(['name' => $cityName]);
        $rayon = $this->getDoctrine()->getRepository(Rayon::class)->findOneBy(['kilometers' => $rayonNumber]);

        $localisation = $chef->getLocalisation();

        if ($localisation) {
          $localisation->setCity($city);
          $localisation->setRayon($rayon);
          $manager = $this->getDoctrine()->getManager();
          $manager->persist($localisation);
          $manager->flush();
        }else {
          $localisation = new Localisation;
          $localisation->setCity($city);
          $localisation->setRayon($rayon);
          $manager = $this->getDoctrine()->getManager();
          $manager->persist($localisation);
          $chef->setLocalisation($localisation);
          $manager->persist($chef);
          $manager->flush();
        }
        return $this->json([
            'success' => true,
            'idLocalisation' => $localisation->getId()
        ]);
    }


    /**
     * @Route("/me/chef/menu/add", name="add_menu", methods={"POST", "OPTIONS"})
     */
    public function addMenu(Request $request)
    {
        $user = $this->getUser();
        $chef = $user->getChef();
        if (!$chef) {
            throw new \Exception("Vous n'êtes pas chef");
        }

        $title = (string) $request->request->get('titleMenu');
        $description = (string) $request->request->get('descriptionMenu');
        $price = (float) $request->request->get('priceMenu');
        if (!$title) {
            throw new NotAcceptableHttpException('Invalid param title');
        }

        if (!$description) {
            throw new NotAcceptableHttpException('Invalid param description');
        }

        $menu = new Menu;
        $menu->setTitle($title);
        $menu->setDescription($description);
        $menu->setChef($chef);
        $menu->setPrice($price);
        $manager = $this->getDoctrine()->getManager();
        $manager->persist($menu);
        $manager->flush();

        return $this->json([
            'success' => true,
            'idMenu' => $menu->getId()
        ]);
    }

    /**
     * @Route("/me/chef/menu/{idMenu}/remove", name="remove_menu", methods={"POST", "OPTIONS"})
     */
    public function removeMenu(Request $request, int $idMenu)
    {
        $user = $this->getUser();
        $chef = $user->getChef();
        if (!$chef) {
            throw new \Exception("Vous n'êtes pas chef");
        }

        $menu = $this->getDoctrine()->getRepository(Menu::class)->findOneBy(['id' => $idMenu]);
        $chefMenus = $chef->getMenus();
        foreach ($chefMenus as $chefMenu) {
            if ($idMenu === $chefMenu->getId()) {
                $removeMenu = $chefMenu;
            }
        }
        if (!$removeMenu) {
            return $this->json([
                'success' => false,
                'message' => "Ce menu n'existe pas."
            ]);
        }

        $chef->removeMenu($removeMenu);
        $starters = $removeMenu->getStarters();
        $dishes = $removeMenu->getDishes();
        $desserts = $removeMenu->getDesserts();
        $diets = $removeMenu->getDiets();
        $categories = $removeMenu->getCategories();

        $manager = $this->getDoctrine()->getManager();
        if ($starters) {
            foreach ($starters as $starter) {
                $manager->remove($starter);
            }
        }

        if ($dishes) {
            foreach ($dishes as $dish) {
                $manager->remove($dish);
            }
        }
        if ($desserts) {
            foreach ($desserts as $dessert) {
                $manager->remove($dessert);
            }
        }

        if ($diets) {
            foreach ($diets as $diet) {
                $removeMenu->removeDiet($diet);
            }
        }
        if ($categories) {
            foreach ($categories as $category) {
                $removeMenu->removeCategory($category);
            }
        }
        $manager->remove($removeMenu);
        $manager->flush();

        return $this->json([
            'success' => true,
        ]);
    }

    /**
     * @Route("/me/chef/menu/edit/{id}", name="edit_menu", methods={"POST", "OPTIONS"})
     */
    public function editMenu(Request $request, int $id)
    {
        $user = $this->getUser();
        $chef = $user->getChef();
        if (!$chef) {
            throw new \Exception("Vous n'êtes pas chef");
        }

        $title = (string) $request->request->get('titleMenu');
        $description = (string) $request->request->get('descriptionMenu');
        $price = (float) $request->request->get('priceMenu');
        if (!$title) {
            throw new NotAcceptableHttpException('Invalid param title');
        }

        if (!$description) {
            throw new NotAcceptableHttpException('Invalid param description');
        }

        $menu = $this->getDoctrine()->getRepository(Menu::class)->findOneBy(['id' => $id, 'chef' => $chef ]);
        $menu->setTitle($title);
        $menu->setDescription($description);
        $menu->setPrice($price);
        $manager = $this->getDoctrine()->getManager();
        $manager->persist($menu);
        $manager->flush();


        return $this->json([
            'success' => true,
            'idMenu' => $menu->getId()
        ]);
    }

    /**
     * @Route("/me/chef/menu/edit/{id}/starter", name="add_menu_starters", methods={"POST", "OPTIONS"})
     */
    public function addMenuStarters(Request $request, int $id)
    {
        $user = $this->getUser();
        $chef = $user->getChef();
        if (!$chef) {
            throw new \Exception("Vous n'êtes pas chef");
        }

        $name = (string) $request->request->get('nameStarter');
        $description = (string) $request->request->get('descriptionStarter');

        if (!$name) {
            throw new NotAcceptableHttpException('Invalid param name');
        }

        if (!$description) {
            throw new NotAcceptableHttpException('Invalid param description');
        }

        $menu =  $this->getDoctrine()->getRepository(Menu::class)->findOneBy(['id' => $id, 'chef' => $chef ]);

        if (!$menu) {
            throw new NotAcceptableHttpException("Ce menu n'existe pas.");
        }
        $starter = new Starter;
        $starter->setName($name);
        $starter->setDescription($description);
        $starter->setMenu($menu);

        $manager = $this->getDoctrine()->getManager();
        $manager->persist($starter);
        $manager->flush();

        return $this->json([
            'success' => true,
            'idMenu' => $menu->getId(),
            'idStarter' => $starter->getId()
        ]);
    }

    /**
     * @Route("/me/chef/menu/edit/{id}/remove/starter", name="remove_starter", methods={"POST", "OPTIONS"})
     */
    public function removeStarter(Request $request, int $id)
    {
        $user = $this->getUser();
        $chef = $user->getChef();
        if (!$chef) {
            throw new \Exception("Vous n'êtes pas chef");
        }
        $menu = $this->getDoctrine()->getRepository(Menu::class)->findOneBy(['id' => $id, 'chef' => $chef ]);

        $idStarter = (int) $request->request->get('idStarter');

        $menuStarters = $menu->getStarters();
        foreach ($menuStarters as $menuStarter) {
            if ($idStarter === $menuStarter->getId()) {
                $removeStarter = $menuStarter;
            }
        }
        if (!$removeStarter) {
            return $this->json([
                'success' => false,
                'message' => "Cette entrée n'existe pas."
            ]);
        }

        $menu->removeStarter($removeStarter);

        $manager = $this->getDoctrine()->getManager();
        $manager->persist($menu);
        $manager->remove($removeStarter);
        $manager->flush();

        return $this->json([
            'success' => true,
        ]);
    }

    /**
     * @Route("/me/chef/menu/edit/{id}/starter/{idStarter}", name="edit_menu_starters", methods={"POST", "OPTIONS"})
     */
    public function editMenuStarters(Request $request, int $id, int $idStarter)
    {
        $user = $this->getUser();
        $chef = $user->getChef();
        if (!$chef) {
            throw new \Exception("Vous n'êtes pas chef");
        }

        $name = (string) $request->request->get('nameStarter');
        $description = (string) $request->request->get('descriptionStarter');

        if (!$name) {
            throw new NotAcceptableHttpException('Invalid param title');
        }

        if (!$description) {
            throw new NotAcceptableHttpException('Invalid param description');
        }

        $menu =  $this->getDoctrine()->getRepository(Menu::class)->findOneBy(['id' => $id, 'chef' => $chef ]);
        if (!$menu) {
            throw new NotAcceptableHttpException("Ce menu n'existe pas.");
        }
        $starter = $this->getDoctrine()->getRepository(Starter::class)->findOneBy(['id' => $idStarter, 'menu' => $menu ]);
        if (!$starter) {
            throw new NotAcceptableHttpException("Cette entrée n'existe pas.");
        }
        $starter->setName($name);
        $starter->setDescription($description);

        $manager = $this->getDoctrine()->getManager();
        $manager->persist($starter);
        $manager->flush();

        return $this->json([
            'success' => true,
            'idMenu' => $menu->getId(),
            'idStarter' => $starter->getId()
        ]);
    }

    /**
     * @Route("/me/chef/menu/edit/{id}/dish", name="add_menu_dish", methods={"POST", "OPTIONS"})
     */
    public function addMenuDish(Request $request, int $id)
    {
        $user = $this->getUser();
        $chef = $user->getChef();
        if (!$chef) {
            throw new \Exception("Vous n'êtes pas chef");
        }

        $name = (string) $request->request->get('nameDish');
        $description = (string) $request->request->get('descriptionDish');

        if (!$name) {
            throw new NotAcceptableHttpException('Invalid param name');
        }
        if (!$description) {
            throw new NotAcceptableHttpException('Invalid param description');
        }

        $menu =  $this->getDoctrine()->getRepository(Menu::class)->findOneBy(['id' => $id, 'chef' => $chef ]);

        if (!$menu) {
            throw new NotAcceptableHttpException("Ce menu n'existe pas.");
        }
        $dish = new Dish;
        $dish->setName($name);
        $dish->setDescription($description);
        $dish->setMenu($menu);

        $manager = $this->getDoctrine()->getManager();
        $manager->persist($dish);
        $manager->flush();

        return $this->json([
            'success' => true,
            'idMenu' => $menu->getId(),
            'idDish' => $dish->getId()
        ]);
    }

    /**
     * @Route("/me/chef/menu/edit/{id}/remove/dish", name="remove_dish", methods={"POST", "OPTIONS"})
     */
    public function removeDish(Request $request, int $id)
    {
        $user = $this->getUser();
        $chef = $user->getChef();
        if (!$chef) {
            throw new \Exception("Vous n'êtes pas chef");
        }
        $menu = $this->getDoctrine()->getRepository(Menu::class)->findOneBy(['id' => $id, 'chef' => $chef ]);

        $idDish = (int) $request->request->get('idDish');

        $menuDishes = $menu->getDishes();
        foreach ($menuDishes as $menuDish) {
            if ($idDish === $menuDish->getId()) {
                $removeDish = $menuDish;
            }
        }
        if (!$removeDish ) {
            return $this->json([
                'success' => false,
                'message' => "Ce plat n'existe pas."
            ]);
        }
        $menu->removeDish($removeDish);

        $manager = $this->getDoctrine()->getManager();
        $manager->persist($menu);
        $manager->remove($removeDish);
        $manager->flush();

        return $this->json([
            'success' => true,
        ]);
    }


    /**
     * @Route("/me/chef/menu/edit/{id}/dish/{idDish}", name="edit_menu_dish", methods={"POST", "OPTIONS"})
     */
    public function editMenuDish(Request $request, int $id, int $idDish)
    {
        $user = $this->getUser();
        $chef = $user->getChef();
        if (!$chef) {
            throw new \Exception("Vous n'êtes pas chef");
        }

        $name = (string) $request->request->get('nameDish');
        $description = (string) $request->request->get('descriptionDish');

        if (!$name) {
            throw new NotAcceptableHttpException('Invalid param title');
        }

        if (!$description) {
            throw new NotAcceptableHttpException('Invalid param description');
        }

        $menu =  $this->getDoctrine()->getRepository(Menu::class)->findOneBy(['id' => $id, 'chef' => $chef ]);
        if (!$menu) {
            throw new NotAcceptableHttpException("Ce menu n'existe pas.");
        }
        $dish = $this->getDoctrine()->getRepository(Dish::class)->findOneBy(['id' => $idDish, 'menu' => $menu ]);
        if (!$dish) {
            throw new NotAcceptableHttpException("Cette entrée n'existe pas.");
        }
        $dish->setName($name);
        $dish->setDescription($description);

        $manager = $this->getDoctrine()->getManager();
        $manager->persist($dish);
        $manager->flush();

        return $this->json([
            'success' => true,
            'idMenu' => $menu->getId(),
            'idDish' => $dish->getId()
        ]);
    }

    /**
     * @Route("/me/chef/menu/edit/{id}/dessert", name="add_menu_dessert", methods={"POST", "OPTIONS"})
     */
    public function addMenuDessert(Request $request, int $id)
    {
        $user = $this->getUser();
        $chef = $user->getChef();
        if (!$chef) {
            throw new \Exception("Vous n'êtes pas chef");
        }

        $name = (string) $request->request->get('nameDessert');
        $description = (string) $request->request->get('descriptionDessert');

        if (!$name) {
            throw new NotAcceptableHttpException('Invalid param name');
        }
        if (!$description) {
            throw new NotAcceptableHttpException('Invalid param description');
        }

        $menu =  $this->getDoctrine()->getRepository(Menu::class)->findOneBy(['id' => $id, 'chef' => $chef ]);

        if (!$menu) {
            throw new NotAcceptableHttpException("Ce menu n'existe pas.");
        }
        $dessert = new Dessert;
        $dessert->setName($name);
        $dessert->setDescription($description);
        $dessert->setMenu($menu);

        $manager = $this->getDoctrine()->getManager();
        $manager->persist($dessert);
        $manager->flush();

        return $this->json([
            'success' => true,
            'idMenu' => $menu->getId(),
            'idDessert' => $dessert->getId()
        ]);
    }

    /**
     * @Route("/me/chef/menu/edit/{id}/remove/dessert", name="remove_dessert", methods={"POST", "OPTIONS"})
     */
    public function removeDessert(Request $request, int $id)
    {
        $user = $this->getUser();
        $chef = $user->getChef();
        if (!$chef) {
            throw new \Exception("Vous n'êtes pas chef");
        }
        $menu = $this->getDoctrine()->getRepository(Menu::class)->findOneBy(['id' => $id, 'chef' => $chef ]);

        $idDessert = (int) $request->request->get('idDessert');

        $menuDesserts = $menu->getDesserts();
        foreach ($menuDesserts as $menuDessert) {
            if ($idDessert === $menuDessert->getId()) {
                $removeDessert = $menuDessert;
            }
        }
        if (!$removeDessert) {
            return $this->json([
                'success' => false,
                'message' => "Ce plat n'existe pas."
            ]);
        }
        $menu->removeDish($removeDessert);

        $manager = $this->getDoctrine()->getManager();
        $manager->persist($menu);
        $manager->remove($removeDessert);
        $manager->flush();

        return $this->json([
            'success' => true,
        ]);
    }

    /**
     * @Route("/me/chef/menu/edit/{id}/dessert/{idDessert}", name="edit_menu_dessert", methods={"POST", "OPTIONS"})
     */
    public function editMenuDessert(Request $request, int $id, int $idDessert)
    {
        $user = $this->getUser();
        $chef = $user->getChef();
        if (!$chef) {
            throw new \Exception("Vous n'êtes pas chef");
        }

        $name = (string) $request->request->get('nameDessert');
        $description = (string) $request->request->get('descriptionDessert');

        if (!$name) {
            throw new NotAcceptableHttpException('Invalid param title');
        }

        if (!$description) {
            throw new NotAcceptableHttpException('Invalid param description');
        }

        $menu =  $this->getDoctrine()->getRepository(Menu::class)->findOneBy(['id' => $id, 'chef' => $chef ]);
        if (!$menu) {
            throw new NotAcceptableHttpException("Ce menu n'existe pas.");
        }
        $dessert = $this->getDoctrine()->getRepository(Dessert::class)->findOneBy(['id' => $idDessert, 'menu' => $menu ]);
        if (!$dessert) {
            throw new NotAcceptableHttpException("Cette entrée n'existe pas.");
        }
        $dessert->setName($name);
        $dessert->setDescription($description);

        $manager = $this->getDoctrine()->getManager();
        $manager->persist($dessert);
        $manager->flush();

        return $this->json([
            'success' => true,
            'idMenu' => $menu->getId(),
            'idDessert' => $dessert->getId()
        ]);
    }

    /**
     * @Route("/me/chef/menu/edit/{id}/add/diet", name="add_menu_diet", methods={"POST", "OPTIONS"})
     */
    public function addMenuDiet(Request $request, int $id)
    {
        $user = $this->getUser();
        $chef = $user->getChef();
        if (!$chef) {
            throw new \Exception("Vous n'êtes pas chef");
        }

        $name = (string) $request->request->get('nameDiet');

        if (!$name) {
            throw new NotAcceptableHttpException('Invalid param name');
        }
        $menu = $this->getDoctrine()->getRepository(Menu::class)->findOneBy(['id' => $id, 'chef' => $chef ]);

        if (!$menu) {
            throw new NotAcceptableHttpException("Ce menu n'existe pas.");
        }
        $diet = $this->getDoctrine()->getRepository(Diet::class)->findOneBy(['name' => $name]);
        if (!$diet) {
            return $this->json([
                'success' => false,
                'message' => "Ce régime alimentaire n'existe pas",
                'idMenu' => $menu->getId()
            ]);
        }
        $menu->addDiet($diet);

        $manager = $this->getDoctrine()->getManager();
        $manager->persist($menu);
        $manager->flush();

        return $this->json([
            'success' => true,
            'idMenu' => $menu->getId()
        ]);
    }

    /**
     * @Route("/me/chef/menu/edit/{id}/remove/diet", name="remove_menu_diet", methods={"POST", "OPTIONS"})
     */
    public function removeMenuDiet(Request $request, int $id)
    {
        $user = $this->getUser();
        $chef = $user->getChef();
        if (!$chef) {
            throw new \Exception("Vous n'êtes pas chef");
        }

        $name = (string) $request->request->get('nameDiet');

        if (!$name) {
            throw new NotAcceptableHttpException('Invalid param name');
        }
        $menu = $this->getDoctrine()->getRepository(Menu::class)->findOneBy(['id' => $id, 'chef' => $chef ]);

        if (!$menu) {
            throw new NotAcceptableHttpException("Ce menu n'existe pas.");
        }
        $diets = $menu->getDiets();
        foreach ($diets as $diet) {
            if ($diet->getName() === $name) {
                $removeDiet = $diet;
            }
        }
        if (!$removeDiet) {
            return $this->json([
                'success' => false,
                'message' => "Ce régime nexiste pas."
            ]);
        }
        $menu->removeDiet($removeDiet);

        $manager = $this->getDoctrine()->getManager();
        $manager->persist($menu);
        $manager->flush();

        return $this->json([
            'success' => true,
            'idMenu' => $menu->getId()
        ]);
    }


    /**
     * @Route("/me/chef/menu/edit/{id}/add/category", name="add_menu_category", methods={"POST", "OPTIONS"})
     */
    public function addMenuCategory(Request $request, int $id)
    {
        $user = $this->getUser();
        $chef = $user->getChef();
        if (!$chef) {
            throw new \Exception("Vous n'êtes pas chef");
        }

        $name = (string) $request->request->get('nameCategory');

        if (!$name) {
            throw new NotAcceptableHttpException('Invalid param name');
        }
        $menu = $this->getDoctrine()->getRepository(Menu::class)->findOneBy(['id' => $id, 'chef' => $chef ]);

        if (!$menu) {
            throw new NotAcceptableHttpException("Ce menu n'existe pas.");
        }
        $category = $this->getDoctrine()->getRepository(Category::class)->findOneBy(['name' => $name]);
        if (!$category) {
            return $this->json([
                'success' => false,
                'message' => "Cette catégorie n'existe pas",
                'idMenu' => $menu->getId()
            ]);
        }
        $menu->addCategory($category);

        $manager = $this->getDoctrine()->getManager();
        $manager->persist($menu);
        $manager->flush();

        return $this->json([
            'success' => true,
            'idMenu' => $menu->getId()
        ]);
    }

    /**
     * @Route("/me/chef/menu/edit/{id}/remove/category", name="remove_menu_category", methods={"POST", "OPTIONS"})
     */
    public function removeMenuCategory(Request $request, int $id)
    {
        $user = $this->getUser();
        $chef = $user->getChef();
        if (!$chef) {
            throw new \Exception("Vous n'êtes pas chef");
        }

        $name = (string) $request->request->get('nameCategory');

        if (!$name) {
            throw new NotAcceptableHttpException('Invalid param name');
        }
        $menu = $this->getDoctrine()->getRepository(Menu::class)->findOneBy(['id' => $id, 'chef' => $chef ]);

        if (!$menu) {
            throw new NotAcceptableHttpException("Ce menu n'existe pas.");
        }
        $categories = $menu->getCategories();
        foreach ($categories as $category) {
            if ($category->getName() === $name) {
                $removeCategory = $category;
            }
        }
        if (!$removeCategory) {
            return $this->json([
                'success' => false,
                'message' => "Cette catégorie nexiste pas."
            ]);
        }
        $menu->removeCategory($removeCategory);

        $manager = $this->getDoctrine()->getManager();
        $manager->persist($menu);
        $manager->flush();

        return $this->json([
            'success' => true,
            'idMenu' => $menu->getId()
        ]);
    }

}
