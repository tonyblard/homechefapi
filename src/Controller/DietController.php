<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpKernel\Exception\NotAcceptableHttpException;

use App\Entity\Diet;

/**
 * @Route("/diets", name="diet_")
 */

class DietController extends AbstractController
{
  /**
   * @Route("/list", name="list", methods={"GET", "OPTIONS"})
   */
  public function allDiets()
  {
      $diets = $this->getDoctrine()->getRepository(Diet::class)->findAll();

      $response = [];
      foreach ($diets as $diet) {
        $response[] = [
          'id' => $diet->getId(),
          'name' => $diet->getName(),
        ];
      }
      return new JsonResponse($response);
  }

}
