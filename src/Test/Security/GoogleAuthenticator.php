<?php

namespace App\Test\Security;

use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Security\Core\User\UserProviderInterface;
use App\Security\GoogleAuthenticator as Authenticator;

class GoogleAuthenticator extends Authenticator
{


    public function supports(Request $request)
    {
      return $this->_supports($request);
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
      return $this->_getUser([
        "sub" => "41245876541036985410",
        "email" => "login.google@test.com",
        "email_verified" => true,
        "name" => "FirstName LastName",
      ]);
    }
}
