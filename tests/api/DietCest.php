<?php namespace App\Tests;
use App\Tests\ApiTester;

class DietCest
{
    public function _before(ApiTester $I)
    {
    }

    // tests
    public function tryToCheckTypesDiets(ApiTester $I)
    {
      $I->haveHttpHeader('Content-Type', 'application/json');
      $I->sendGET('/diets/list');
      $I->seeResponseIsJson();
      $I->seeResponseMatchesJsonType([
        'id' => 'integer',
        'name' => 'string',
      ]);
    }
}
