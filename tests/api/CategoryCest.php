<?php namespace App\Tests;
use App\Tests\ApiTester;

class CategoryCest
{
    public function _before(ApiTester $I)
    {
    }

    // tests
    public function tryToCheckTypesCategories(ApiTester $I)
    {
      $I->haveHttpHeader('Content-Type', 'application/json');
      $I->sendGET('/categories/list');
      $I->seeResponseIsJson();
      $I->seeResponseMatchesJsonType([
        'id' => 'integer',
        'name' => 'string',
      ]);
    }
}
