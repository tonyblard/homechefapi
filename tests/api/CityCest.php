<?php namespace App\Tests;
use App\Tests\ApiTester;

class CityCest
{
    public function _before(ApiTester $I)
    {
    }

    // tests
    public function tryToCheckTypesCities(ApiTester $I)
    {
      $I->haveHttpHeader('Content-Type', 'application/json');
      $I->sendGET('/city/list');
      $I->seeResponseIsJson();
      $I->seeResponseMatchesJsonType([
        'id' => 'integer',
        'name' => 'string',
      ]);
    }
}
