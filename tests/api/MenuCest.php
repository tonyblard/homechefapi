<?php
namespace App\Tests;
use App\Tests\ApiTester;

class MenuCest
{
    public function _before(ApiTester $I)
    {
    }

    // tests
    public function tryToCheckTypesMenus(ApiTester $I)
    {
      $I->haveHttpHeader('Content-Type', 'application/json');
      $I->sendGET('/menu');
      $I->seeResponseIsJson();
      $I->seeResponseMatchesJsonType([
        'id' => 'integer',
        'chef_id' => 'integer',
        'title' => 'string',
        'description' => 'string',
        'price' => 'integer|float',
      ]);
    }
}
